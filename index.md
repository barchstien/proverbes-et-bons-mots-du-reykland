---
title: Proverbes et Bons Mots du Reykland
layout: default
---

## 22 Januaringen de l'an de grâce 2023
- J'ai beau être matinal, j'ai mal - souvent évoqué au lendemain d'un acte héroique... ou d'un manque de prudence

## 9 Januaringen de l'an de grâce 2023
- Voir le pont de Fielbach et mourir (chef d'oeuvre de l'architecture naine prouvant une fois pour toute leur supériorité sur la nature)

## 21 Novembriung de l'an de grâce 2022
- Tire la chevillette et la bobinette chierra

## 11 Oktobriung de l'an de grâce 2022
- En pflugzeit ne te découvre pas d'un pfilgzeit

## 26 Zebtembriung de l'an de grâce 2022
- A Weissbrück on fait comme les Weissbrückien.nes
- Chacun chez soi et les mutants seront bien gardés

